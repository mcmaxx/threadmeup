

// Used to manage, massage, return our data
class DataStore {
  constructor(initialData) {
    this._data = initialData;
  }

  get(...keys) {
    if (keys.length === 0) {
      return this._data;
    }
  }

  groupBy(category, countType) {
    let grouping = {};
    let toAdd = 1;
    let indexCategory = '';
    let month = 0;
    let year = 0;
    let date = null;

    _.each(this._data, function(value) {
      toAdd = 1;

      if (category !== "date") {
        indexCategory = value[category];
      }
      else {
        date = new Date(value[category]);
        month = date.getUTCMonth() + 1;
        year = date.getFullYear();
        indexCategory = `${month}-${year}`;
      }

      if (countType) {
        toAdd = parseInt(value[countType]);
      }

      if (grouping[indexCategory]) {
        grouping[indexCategory] += toAdd;
      }
      else {
        grouping[indexCategory] = toAdd;
      }
    });
   
    return grouping;
  }

  sortBy(category, set) {
    let sortedData = _.sortBy(this._data, function(o) { return o[category]});

    return sortedData;
  }
}

// appears on top of the table when viewed
var TableFilters = React.createClass({
  propTypes: {
    dataStore: React.PropTypes.instanceOf(DataStore),
    setSelectedFilters: React.PropTypes.func.isRequired,
    selectedFilters: React.PropTypes.array.isRequired,
    tableKeys: React.PropTypes.array.isRequired,
  },
  handleTableFilter(key){
    let selectedFilters = this.props.selectedFilters;
    let index = selectedFilters.indexOf(key);

    if (index === -1) {
      selectedFilters.push(key);
    }
    else {
      selectedFilters.splice(index, 1)
    }

    this.props.setSelectedFilters(selectedFilters);
    
  },
  render() {
    return (
      <span>
        <div className="btn-group">
        {this.props.tableKeys.map(droneKey =>
          <button 
            key={droneKey}   
            type="button"
            className={classNames({
              'btn-success': this.props.selectedFilters.indexOf(droneKey) !== -1?true:false,
            })}
            onClick={this.handleTableFilter.bind(this, droneKey)}
            type="button"
          >
            {droneKey}
          </button>
        )}
        </div>
      </span>
    )
  },
})


var DroneRow = React.createClass({
  propTypes: {
    selectedFilters: React.PropTypes.array.isRequired,
    strike: React.PropTypes.object.isRequired,
  },
  render() {
    return (
      <tr>
        {this.props.selectedFilters.map(filter => 
        <td
          key={filter}
        >
          {this.props.strike[filter]}
        </td>
        )}
      </tr>
    )
  },
});

var DroneTable = React.createClass({
  propTypes: {
    dataStore: React.PropTypes.instanceOf(DataStore),
  },
  getInitialState () {
    return {
      selectedFilters: ['number', 'narrative', 'deaths', 'date'],

    }
  },
  setSelectedFilters(selectedFilters) {
    this.setState({'selectedFilters': selectedFilters});
  },
  render () {
    return (
      <div>
        <div className="btn-group">
          <TableFilters
            dataStore={this.props.dataStore}
            selectedFilters={this.state.selectedFilters}
            tableKeys={_.keys(this.props.dataStore.get()[0])}
            setSelectedFilters={this.setSelectedFilters}
          />
        </div>
        <table 
          className="table table-condensed table-hover"
        >
          <thead>
            <tr>
              {this.state.selectedFilters.map(filter =>
                <th
                  key={filter}
                >
                  {filter}
                </th>
              )}
            </tr>
          </thead>
          <tbody>
            {this.props.dataStore.get().map(strike =>
              <DroneRow
                key={strike._id}
                dataStore={this.props.dataStore}
                selectedFilters={this.state.selectedFilters}
                strike={strike}
              />
            )}
          </tbody>
        </table>
      </div>
    );
  }
});

// nasty referring to 'google' like this because of scope, I'm sure they have react-ish libraries
google.charts.load('current', {packages: ['corechart', 'bar']});

var DroneGraph = React.createClass({
  barChart: null,
  pieChart: null,
  propTypes: {
    dataStore: React.PropTypes.instanceOf(DataStore),
  },
  componentDidMount() {
    let self = this;
    let countryGrouping = this.props.dataStore.groupBy('country', 'deaths');
    let monthlyGrouping = this.props.dataStore.groupBy('date');

    // pie chart data
    countryGrouping = $.map(countryGrouping, function(value, index) {
      return [[index, value]];
    });
    // bar graph data
    monthlyGrouping = $.map(monthlyGrouping, function(value, index) {
      return [[index, value]];
    });

    function drawChart() {
      // Define the chart to be drawn.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Country');
      data.addColumn('number', 'Deaths');
      data.addRows(countryGrouping);

      // Instantiate and draw the pie chart.
      self.pieChart = new google.visualization.PieChart(document.getElementById('pie-chart'));
      self.pieChart.draw(data, null);

      // Define the chart to be drawn.
      monthlyGrouping.unshift(['date', 'reports'])
 
      data = new google.visualization.arrayToDataTable(monthlyGrouping);

      // Instantiate and draw the chart.
      self.barChart = new google.visualization.ColumnChart(document.getElementById('bar-chart'));
      self.barChart.draw(data);
    }

    google.charts.setOnLoadCallback(drawChart);
  },
  // clear up some of that memory
  componentWillUnmount() {
    this.pieChart.clearChart();
    this.barChart.clearChart();
  },
  render () {
    return (
      <div>
        <div className="row">
          <div className="col-sm-12">
            <h1>Deaths by Country</h1>
            <div id="pie-chart"></div>
          </div>
        </div>
        <div className="col-sm-12">
            <h1>Number of Incidents by Date</h1>
            <div id="bar-chart"></div>
        </div>
      </div>
    );
  }
});

var DroneNav = React.createClass({
  getInitialState() {
    return {
      tabSelected: 'table',
      dataStore: null,
    }
  },
  componentDidMount() {
    // component's mounted lets fetch some data
    // would be nice to fetch real data but x-origin blocked
    let self = this;

    //here we could do some kind of 'loading', 'failed', if it wasn't blocked x-origin
    $.get('http://threadmeup.mcmaxxlfg.webfactional.com/data.json', function(data) {
      if (data.status === 'OK') {
        self.setState({
          'dataStore': new DataStore(data.strike),
        });
      }
    });
  },
  navbarClick(tab) {
    this.setState({tabSelected:tab});
  },
  render() {
    return (
      <div className="container">
        <nav className="navbar navbar-inverse"> 
          <div className="container-fluid">  
            <div className="navbar-header"> 
              <button type="button" 
                className="navbar-toggle collapsed" 
                data-toggle="collapse" 
                data-target="#bs-example-navbar-collapse-9" 
                aria-expanded="false"> 
                <span className="sr-only">Toggle navigation</span> 
                <span className="icon-bar"></span> 
                <span className="icon-bar"></span> 
                <span className="icon-bar"></span> 
              </button> 
              <a className="navbar-brand" href="#">Drone Stats</a> 
            </div>  
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-9"> 
              <ul className="nav navbar-nav"> 
                <li 
                  className={classNames({
                    'active': this.state.tabSelected === 'table'?true:false,
                  })}
                  onClick={this.navbarClick.bind(this, 'table')}
                >
                  <a href="#">Table</a>
                </li> 
                <li 
                  className={classNames({
                    'active': this.state.tabSelected === 'graph'?true:false,
                  })}
                  onClick={this.navbarClick.bind(this, 'graph')}
                >
                  <a href="#">Graphs</a>
                </li> 
              </ul> 
            </div> 
          </div> 
        </nav>
        <hr/>
        {this.state.dataStore ? (
          <div>
            <div>
              {this.state.tabSelected === 'table' ? (
                <DroneTable
                  dataStore={this.state.dataStore}
                />
              ): null}
            </div>
            <div>
              {this.state.tabSelected === 'graph' ? (
                <DroneGraph
                  dataStore={this.state.dataStore}
                />
              ): null}
            </div>
          </div>
        ): null}
      </div>
    )
  },
});

$(() => {
  ReactDOM.render(
    <DroneNav />,
    document.getElementById('content')
  );
});


